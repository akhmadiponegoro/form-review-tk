from django.shortcuts import render, redirect
from .models import Review
from . import forms

def review(request):
    reviews = Review.objects.all().order_by('id')
    return render(request, 'result.html', {'reviews': reviews})

def review_create(request):
    if request.method == 'POST':
        form = forms.ReviewForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('homepage:review')

    else:
        form = forms.ScheduleForm()
    return render(request, 'input.html', {'form': form})