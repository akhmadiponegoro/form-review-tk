from django.urls import path
from . import views

app_name = 'homepage'

urlpatterns = [
    path('', views.review, name='review'),
    path('review/create', views.review_create, name='review_create'),
]