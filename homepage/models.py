from django.db import models


class Review(models.Model):

    fill = models.CharField(max_length=200)

    def __str__(self):
        return "{}. {}".format(self.id, self.activity)
